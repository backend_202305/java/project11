package wrapper.quiz;

/*
 * byte, char, short, long, float, double의 wrapper 클래스형 변수를 선언하세요.
 * 그리고 각 변수에 알맞은 값을 대입하세요
 * */
public class Quiz1 {

	public static void main(String[] args) {
		
		Byte bNum = 1;
		Character ch = 'a';
		Short sNum = 1;
		Long lNum = 1l;
		Float fNum = 1.0f;
		Double dNum = 1.0;

	}
}
